certifi==2018.10.15
chardet==3.0.4
Django==2.1.1
django-cors-headers==2.4.0
django-debug-toolbar==1.10.1
django-filter==2.0.0
django-oauth-toolkit==1.2.0
djangorestframework==3.8.2
idna==2.7
Markdown==3.0.1
oauthlib==2.1.0
psycopg2==2.7.5
psycopg2-binary==2.7.5
pytz==2018.5
requests==2.19.1
sqlparse==0.2.4
urllib3==1.23

