from django.urls import include, path

from .views import (
    #Bill
    BillAddProductView,
    BillCreateView,
    BillDetailsView,
    BillListView,
    #Contact
    ContactCreateView,
    ContactDeleteView,
    ContactDetailsView,
    ContactUpdateView,
    ContactListView,
    #Product
    ProductCreateView,
    ProductDeleteView,
    ProductListView,
    ProductUpdateView,
    #Shop
    ShopCreateView,
    ShopDeleteView,
    ShopListView,
    ShopUpdateView,
)

app_name = 'managebill'

urlpatterns = [
    #Add product to a bill
    path('bill/add/product/<int:id>', BillAddProductView.as_view(), name='bill_add_product'),
    #Bill's create url
    path('bill/create/', BillCreateView.as_view(), name='bill_create'),
    #Bill details url
    path('bill/details/<int:id>', BillDetailsView.as_view(), name='bill_details'),
    #Bill list url
    path('bill/', BillListView.as_view(), name='bills_list'),
    #Contact's urls
    path('contact/', ContactListView.as_view(), name='contacts'),
    # Create a contact
    path('contact/create/', ContactCreateView.as_view(), name='contact_create'),
    #Contact's delete url
    path('contact/delete/<int:id>', ContactDeleteView.as_view(), name='contact_delete'),
    #Contact's details url
    path('contact/details/<int:id>', ContactDetailsView.as_view(), name='contact_details'),
    #Contact's update url
    path('contact/update/<int:id>', ContactUpdateView.as_view(), name='contact_update'),
    #Product's create url
    path('product/create/', ProductCreateView.as_view(), name='product_create'),
    #Product's delete url
    path('product/delete/<int:id>', ProductDeleteView.as_view(), name='product_delete'),
    #Product's update url
    path('product/update/<int:id>', ProductUpdateView.as_view(), name='product_update'),
    #Products url
    path('product/', ProductListView.as_view(), name='products'),
    #Shop's create url
    path('shop/create/', ShopCreateView.as_view(), name='shop_create'),
    #Shop's delete url
    path('shop/delete/<int:id>', ShopDeleteView.as_view(), name='shop_delete'),
    #Shops list url
    path('shop/', ShopListView.as_view(), name='shops_list'),
    #Shop's update url
    path('shop/update/<int:id>', ShopUpdateView.as_view(), name='shop_update'),
    #path('user/', UserView.as_view(), name='user'),
    #User's login url
]
