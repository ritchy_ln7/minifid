from django.forms import ModelForm, Select

from .models import  Bill, BillProduct, Contact, Product, Shop


class BillAddProductForm(ModelForm):
    """
    Implements the creation of product to a bill.
    """
    class Meta:
        model = BillProduct
        fields = [
            'bill',
            'product',
            'quantity',
        ]
        widgets = {
            'bill': Select(attrs={
                'disabled': 'true'
                })
        }


class BillForm(ModelForm):
    """
    Implements the creation bill form.
    """
    class Meta:
        model = Bill
        fields = [
            'ticket_number',
            'contact',
            'shop',
        ]


class ContactForm(ModelForm):
    """
    Implements the creation contact form.
    """
    class Meta:
        model = Contact
        fields = [
            'first_name',
            'last_name',
        ]


class ProductForm(ModelForm):
    """
    Implements the creation product form.
    """
    class Meta:
        model = Product
        fields = [
            'name',
        ]


class ShopForm(ModelForm):
    """
    Implements the creation shop form.
    """
    class Meta:
        model = Shop
        fields = [
            'name',
            'address',
        ]
