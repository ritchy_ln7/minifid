from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LogoutView
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, get_object_or_404
from django.urls import reverse, reverse_lazy
from django.views import View
from django.views.generic import (
    CreateView,
    DeleteView,
    DetailView,
    ListView,
    TemplateView,
    UpdateView,
)

from .forms import BillAddProductForm, BillForm, ContactForm, ProductForm, ShopForm
from .models import Shop, Contact, Bill, BillProduct, Product


class BillAddProductView(LoginRequiredMixin, CreateView):
    """
    Implements the creation of a product line to a bill.
    """
    form_class = BillAddProductForm
    bill_id = 0
    success_url = reverse_lazy('managebill:bills_list')
    template_name = 'managebill/bill/add-product.html'

    def form_valid(self, form):
        """
        Security check.
        """
        return super().form_valid(form)

    def dispatch(self, request, *args, **kwargs):
        """
        Overrides dispatch to ....
        """
        _id = kwargs['id']
        self.bill_id = Bill.objects.filter(ticket_number=_id).get().id
        return super().dispatch(request, *args, **kwargs)

    def get_initial(self):
        """
        Overrides get_initial method to populate bill id in form field.
        """
        initial = super(BillAddProductView, self).get_initial()
        initial['bill'] = self.bill_id
        return initial

    def post(self, request, **kwargs):
        """
        Overrides post method to ......
        """
        request.POST = request.POST.copy()
        request.POST['bill'] = self.bill_id
        return super(BillAddProductView, self).post(request, **kwargs)


class BillCreateView(LoginRequiredMixin, CreateView):
    """
    Implements the creation of a bill.
    """
    template_name = 'managebill/bill/create.html'
    form_class = BillForm
    success_url = reverse_lazy('managebill:bills_list')
    queryset = Bill.objects.all()

    def form_valid(self, form):
        """
        Security check.
        """
        return super().form_valid(form)


class BillDetailsView(LoginRequiredMixin, DetailView):
    """
    Implements the view of all products on a bill.
    """
    template_name = 'managebill/bill/details.html'
    context_object_name = 'obj'

    def get_object(self):
        """
        Get the bill's product list
        """
        id_ = self.kwargs.get('id')
        response = Bill.objects.filter(ticket_number=id_).get()
        return response


class BillListView(LoginRequiredMixin, ListView):
    """
    Implements the view of all bills
    """
    template_name = 'managebill/bill/list.html'
    queryset = Bill.objects.all().order_by('contact_id')
    context_object_name = 'obj'


class ContactCreateView(LoginRequiredMixin, CreateView):
    """
    Implements the creation of a contact.
    """
    template_name = 'managebill/contact/create.html'
    form_class = ContactForm
    queryset = Contact.objects.all()

    def form_valid(self, form):
        """
        Security check.
        """
        return super().form_valid(form)


class ContactDeleteView(LoginRequiredMixin, DeleteView):
    """
    Implements the view of the contact's delete.
    """
    template_name = 'managebill/contact/delete.html'
    context_object_name = 'obj'

    def get_object(self):
        """
        Get the contact id to delete.
        """
        id_ = self.kwargs.get('id')
        obj = Contact.objects.filter(id=id_).get()
        return obj

    def get_success_url(self):
        """
        When you delete the contact you are redirected to the contact's list url
        """
        response = reverse('managebill:contacts')
        return response


class ContactUpdateView(LoginRequiredMixin, UpdateView):
    """
    Implements the view of the contact's update
    """
    template_name = 'managebill/contact/update.html'
    form_class = ContactForm
    queryset = Contact.objects.all()
    context_object_name = 'obj'

    def get_object(self):
        """
        Get the contact id to delete.
        """
        id_ = self.kwargs.get('id')
        obj = Contact.objects.filter(id=id_).get()
        return obj

    def form_valid(self, form):
        """
        Security check.
        """
        return super().form_valid(form)


class ContactDetailsView(LoginRequiredMixin, DetailView):
    """
    Implements the view to list all contact's bills.
    """
    template_name = 'managebill/contact/details.html'
    context_object_name = 'obj'

    def get_object(self):
        """
        Get the contact's bill list
        """
        id_ = self.kwargs.get('id')
        response = Bill.objects.filter(contact_id=id_)
        return response


class ContactListView(LoginRequiredMixin, ListView):
    """
    Implements the view to list all contacts.
    """
    context_object_name = 'obj'
    template_name = 'managebill/contact/list.html'
    queryset = Contact.objects.all()


class IndexView(LoginRequiredMixin, TemplateView):
    """
    Implements the view of the index
    """
    template_name = 'managebill/index.html'


class myLogoutView(LogoutView):
    """
    Log out the user and display the 'You are logged out' message.
    """
    next_page = 'login'


class ProductCreateView(LoginRequiredMixin, CreateView):
    """
    Implements the creation of a product
    """
    form_class = ProductForm
    queryset = Product.objects.all()
    success_url = reverse_lazy('managebill:products')
    template_name = 'managebill/product/create.html'


class ProductDeleteView(LoginRequiredMixin, DeleteView):
    """
    Implements the view of the product's delete.
    """
    success_url = reverse_lazy('managebill:products')
    template_name = 'managebill/product/delete.html'
    context_object_name = 'obj'

    def get_object(self):
        """
        Get the product id to delete.
        """
        id_ = self.kwargs.get('id')
        obj = Product.objects.filter(id=id_).get()
        return obj


class ProductListView(LoginRequiredMixin, ListView):
    """
    Implements the view for all products available
    """
    context_object_name = 'obj'
    template_name = 'managebill/product/list.html'
    queryset = Product.objects.all().order_by('name')

    def form_valid(self, form):
        """
        TODO
        """
        return super().form_valid(form)


class ProductUpdateView(LoginRequiredMixin, UpdateView):
    """
    Implements the view of the contact's update
    """
    context_object_name = 'obj'
    form_class = ProductForm
    queryset = Product.objects.all()
    success_url = reverse_lazy('managebill:products')
    template_name = 'managebill/product/update.html'

    def get_object(self):
        """
        Get the contact id to delete.
        """
        id_ = self.kwargs.get('id')
        obj = Product.objects.filter(id=id_).get()
        return obj

    def form_valid(self, form):
        """
        Security check.
        """
        return super().form_valid(form)


class ShopCreateView(LoginRequiredMixin, CreateView):
    """
    Implements the creation of a shop
    """
    form_class = ShopForm
    queryset = Shop.objects.all()
    success_url = reverse_lazy('managebill:shops_list')
    template_name = 'managebill/shop/create.html'


class ShopDeleteView(LoginRequiredMixin, DeleteView):
    """
    Implements the view of the shop's delete.
    """
    template_name = 'managebill/shop/delete.html'
    context_object_name = 'obj'

    def get_object(self):
        """
        Get the shop id to delete.
        """
        id_ = self.kwargs.get('id')
        obj = Shop.objects.filter(id=id_).get()
        return obj

    def get_success_url(self):
        """
        When you delete the contact you are redirected to the contact's list url
        """
        response = reverse('managebill:shops_list')
        return response


class ShopListView(LoginRequiredMixin, ListView):
    """
    Implements the view for all shops
    """
    context_object_name = 'obj'
    template_name = 'managebill/shop/list.html'
    queryset = Shop.objects.all().order_by('name')


class ShopUpdateView(LoginRequiredMixin, UpdateView):
    """
    Implements the view of the shop's update
    """
    context_object_name = 'obj'
    form_class = ShopForm
    queryset = Shop.objects.all()
    success_url = reverse_lazy('managebill:shops_list')
    template_name = 'managebill/shop/update.html'

    def get_object(self):
        """
        Get the shop id to delete.
        """
        id_ = self.kwargs.get('id')
        obj = Shop.objects.filter(id=id_).get()
        return obj

    def form_valid(self, form):
        """
        Security check.
        """
        return super().form_valid(form)
