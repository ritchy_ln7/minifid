import os

from django.core.exceptions import ImproperlyConfigured


def get_env_variable(var_name):
    """
    Get the environment variable or return exception.
    """
    try:
        return os.environ[var_name]
    except KeyError as e:
        print('---\n An error occured attempting to get variable environnement :\n')
        print('{}\n---'.format(e))
        raise ImproperlyConfigured('Set the {} environment variable'.format(var_name))
