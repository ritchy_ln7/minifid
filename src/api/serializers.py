from rest_framework.serializers import (
    HyperlinkedIdentityField,
    ModelSerializer
)

from managebill.models import Bill, Contact


class BillCreateSerializer(ModelSerializer):
    """
    Serialize the bill create model
    Extends:
        ModelSerializer
    """
    class Meta:
        model = Bill
        fields = [
            'ticket_number',
            'contact',
            'shop',
        ]


class BillSerializer(ModelSerializer):
    """
    Serialize the bill model
    Extends:
        ModelSerializer
    """
    class Meta:
        model = Bill
        fields = [
            'id',
            'ticket_number',
            'contact',
            'shop',
        ]


class BillUpdateSerializer(ModelSerializer):
    """
    Serialize the bill model
    Extends:
        ModelSerializer
    """

    class Meta:
        model = Bill
        fields = [
            'contact',
            'shop',
        ]


class ContactSerializer(ModelSerializer):
    """
    Serialize the contact model
    Extends:
        ModelSerializer
    """

    class Meta:
        model = Contact
        fields = [
            'first_name',
            'last_name',
        ]
